/**
 * 
 */
package org.prelle.splimo.modifications;

import org.prelle.simplepersist.EnumValue;

/**
 * @author prelle
 *
 */
public enum PointsModType {

	@EnumValue("power")
	POWER,
	
}
