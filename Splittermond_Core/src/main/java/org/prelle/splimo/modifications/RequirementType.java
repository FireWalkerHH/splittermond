/**
 * 
 */
package org.prelle.splimo.modifications;

import org.prelle.simplepersist.EnumValue;

/**
 * @author prelle
 *
 */
public enum RequirementType {
	
	@EnumValue("culture")
	CULTURE,
	@EnumValue("gender")
	GENDER,
	@EnumValue("race")
	RACE,
	@EnumValue("featuretype")
	FEATURE_TYPE,

}
