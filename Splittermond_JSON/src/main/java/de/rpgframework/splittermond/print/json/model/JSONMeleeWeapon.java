package de.rpgframework.splittermond.print.json.model;

import java.util.List;

public class JSONMeleeWeapon {
    public String name;
    public String skill;
    public String attribute1;
    public String attribute2;
    public int value;
    public List<JSONFeature> features;
    public String damage;
    public int weaponSpeed;
    public int characterTickMalus;
    public int calculatedSpeed;
    public boolean relic;
    public boolean personalized;
}
