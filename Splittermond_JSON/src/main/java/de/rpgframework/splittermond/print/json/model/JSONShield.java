package de.rpgframework.splittermond.print.json.model;

import java.util.List;

public class JSONShield {
    public String name;
    public int activeDefenseValue;
    public String skill;
    public int attack;
    public String damage;
    public int defensePlus;
    public int handicap;
    public int tickMalus;
    public List<JSONFeature> features;
    public boolean relic;
    public boolean personalized;
}
