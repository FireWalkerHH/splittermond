package de.rpgframework.splittermond.print.json.model;

public class JSONSpell {
    public String name;
    public String id;
    public int value;
    public String school;
    public int schoolGrade;
    public String difficulty;
    public String focus;
    public String castDuration;
    public String castRange;
    public String spellDuration;
    public String enhancement;
    public String page;
    public String longDescription ="";
}
