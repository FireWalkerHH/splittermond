package de.rpgframework.splittermond.print.json.model;

public class JSONPower {
    public String name;
    public String id;
    public int count;
    public String shortDescription;
    public String longDescription;
    public String page;
}
