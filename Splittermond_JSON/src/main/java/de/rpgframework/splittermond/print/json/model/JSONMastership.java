package de.rpgframework.splittermond.print.json.model;

public class JSONMastership {
    public String name;
    public String id;
    public int level;
    public String shortDescription;
    public String longDescription;
    public String page;
}
