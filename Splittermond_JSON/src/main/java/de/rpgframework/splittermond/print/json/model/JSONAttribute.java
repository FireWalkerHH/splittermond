package de.rpgframework.splittermond.print.json.model;

public class JSONAttribute {
    public String name;
    public String shortName;
    public int startValue;
    public int value;
}
