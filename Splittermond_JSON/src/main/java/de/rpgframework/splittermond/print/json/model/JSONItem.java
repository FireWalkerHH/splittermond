package de.rpgframework.splittermond.print.json.model;

public class JSONItem {
    public String name;
    public int count;
    public boolean relic;
    public boolean personalized;
}
